**How this encryption works**

First the client and server has a static key.

Second by using the static key the client will call "validateBusiness API" and the encryption and decryption was done by static key.

Then the server will send the public and private key for further encryption.

## Further encryption.

After getting the public and private key the client will generate a random key and encrypt the payload by using the random key (AES)

Then the client will encrypt the random key by using the public key sent by the server (RSA)

Now both the key and the payload was encrypted and send to the server as {"encryptedKey":"","encryptedPayload":""}

The server will decrypt the payload and send the response as {"encryptedKey":"","encryptedPayload":""}

Then the client will decrypt the encryptedKey by the primary key which is sent by the server (RSA) and decrypt the payload after getting the key (AES)



**This library has embedded the network call with encryption and decryption for request and response**

Purpose of this library is to avoid the code addition for encryption and decryption for request and response in network call

By simply calling this library it can handle those encryption and decryption part

-------------------------------------------------------------------------------------


## Usage of this library

Simply add this code like below

 
 ## this case is for with encryption

        val m2PNetworkCall = M2PNetworkCall
            .Builder()
            .setBaseUrl(ForexBaseUrl)
            .setStaticPrimaryKey(STATIC_PRIMARYCONTENT)
            .setOkHttpClient(okHttpClient)
            .setContext(applicationContext)
            .build()

apiInterface = m2PNetworkCall.getRetrofitBuild(true).create(ApiInterface::class.java)

-------------------------------------------------------------------------------------

 ## this case is for without encryption

       
	  val m2PNetworkCall = M2PNetworkCall
            .Builder()
            .setBaseUrl(ForexBaseUrl)
            .setOkHttpClient(okHttpClient)
            .build()

apiInterface = m2PNetworkCall.getRetrofitBuild(false).create(ApiInterface::class.java)

-------------------------------------------------------------------------------------


 ## this snippet is to set the value of public and private key
 
 
 M2PValuePrefs(applicationContext).setValues(privateKey, publicKey)
 
 -------------------------------------------------------------------------------------
 
 
 ## Class holds secure layer logic

class CustomConvertorFactory holds the logic for encryption and decryption 
